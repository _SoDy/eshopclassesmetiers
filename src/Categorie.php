<?php

namespace eshopClassesMetiers ;

class Categorie {

    private $idCategorie;
    private $libelle;
    private $image;
    private $collectionProduit;

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function getIdCategorie() {
        return $this->idCategorie;
    }

    public function getLibelle() {
        return $this->libelle;
    }

    public function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    public function addProduit(Produit $produit) {
        if (!$this->existProduit($produit)) {
            $this->collectionProduit[] = $produit;
            if (!$produit->getCategorie()->compareTo($this)) {
                $produit->setCategorie($this);
            }
        }
    }

    public function removeProduit(Produit $produit) {
        $new = array();
        foreach ($this->collectionProduit as $produitCourant) {
            if (!$produitCourant->compareTo($produit)) {
                $new[] = $produitCourant;
                break;
            }
        }
        $this->collectionProduit = $new;
        $produit->setCategorie(null);
    }

    public function compareTo(Categorie $categorie) {
        return $this->idCategorie == $categorie->idCategorie;
    }

    private function existProduit(Produit $produit) {
        $existe = false;
        foreach ($this->collectionProduit as $produitCourant) {
            if ($produit->compareTo($produitCourant)) {
                $existe = true;
                break;
            }
        }
        return $existe;
    }

    private static $select = "select * from categorie ";
    private static $selectById = "select * from categorie where idCategorie=:idCategorie";
    private static $insert = "insert into categorie (libelle, image) values (:libelle,:image)";
    private static $update = "update categorie set libelle=:libelle, image=:image where idCategorie = :idCategorie";

    public function save() {
        if ($this->idCategorie == null) {
            $this->insert();
        } else {
            $this->update();
        }
        $this->saveProduits();
    }

    private function saveProduits() {
        foreach ($this->collectionProduit as $produit) {
                $produit->save();
        }
    }

    public function insert() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
        $this->idCategorie = $pdo->lastInsertId();
    }

    public function update() {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$update);
        $pdoStatement->bindParam("idCategorie", $this->idCategorie);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->bindParam(":image", $this->image);
        $pdoStatement->execute();
    }

    public static function fetch($idCategorie) {
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Categorie::$selectById);
        $pdoStatement->bindParam(":idCategorie", $idCategorie);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $categorie = Categorie::arrayToCategorie($record);
        return $categorie;
    }

    public static function fetchAll() {
        $categories = array();
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->query(Categorie::$select);
        $resultat = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        if (is_array($resultat)) {
            foreach ($resultat as $value) {
                $categories[] = Categorie::arrayToCategorie($value);
            }
        }
        return $categories;
    }

    private static function arrayToCategorie(Array $array) {
        $c = new Categorie();
        $c->idCategorie = $array["idCategorie"];
        $c->libelle = $array["libelle"];
        $c->image = $array["image"];
        $c->collectionProduit = Produit::fetchAllByCategorie($c);
        return $c;
    }

}
